#ifndef _UTILS_H_
#define _UTILS_H_
#include "utils.c"
void rewrite_new_bmp(char*, char*, struct image*);
uint32_t le_to_normal_4B(uint32_t);
uint16_t le_to_normal_2B(uint16_t);
char* add_caption(char*, const char*);
int check(char*);
void create_file(char*);
#endif