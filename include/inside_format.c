#ifndef _INSIDE_FORMAT_C_
#define _INSIDE_FORMAT_C_
#include <stdint.h>

#pragma pack(1)
struct pixel
{
    uint8_t b,g,r;
};

struct image //max abstraction from type of file
{
    uint32_t width, heigth;
    struct pixel* data;
};

//struct bmp_file_header
//{
//    uint16_t signature;
//    uint32_t file_size;
//    uint32_t reserved;
//    uint32_t file_offset;
//};

struct DIB_header_bmp_file
{
    uint32_t DIB_header_size;
    uint32_t width,heigth;
    //next information will be skipped, because it doesnt matter
};


struct bmp_file_header
{
    //bmp-header
    uint16_t signature;
    uint32_t file_size;
    uint16_t reserved1,reserved2;
    uint32_t offset;//54 причина описанна ниже
    //bmpinfo-header(DIB)
    uint32_t dib_size;
    uint32_t width;
    uint32_t heigth;
    uint16_t planes;//0
    uint16_t bitcount;// 24 по идее кол-во битов на точку
    uint32_t compression;//0 тк мы поддреживаем только 24битные цветовые пространства
    uint32_t image_size;// размер изображения в байтах учитывая padding
    uint32_t XpelsPerMeter;//0
    uint32_t YpelsPerMeter;//0
    uint32_t colorsused;//0
    uint32_t colorsimportant;//0
    //color-table(optional)
    //uint32_t colortable[256];//commented because we support only 24bit pixel structure
    //bitmap-array
    uint8_t* ImageData;
};
#pragma pop

#endif