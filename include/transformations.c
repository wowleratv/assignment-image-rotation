#ifndef _TRANSFOIMATIONS_C_
#define _TRANSFOIMATIONS_C_

#include <malloc.h>
#include "inside_format.h"

struct image* image_rotation(struct image* picture)
{
    uint32_t h = (*picture).heigth;
    uint32_t w = (*picture).width;
    struct image* rotated_img = (struct image*)malloc(1*sizeof(struct image)); //https://tenor.com/ru/view/get-rotated-get-rotated-idiot-rotate-rotation-shark-gif-24511062
    struct pixel* rotated_data = (struct pixel*)malloc((*picture).width * (*picture).heigth * sizeof(struct pixel));
    struct pixel data1[h][w], data2[w][h];

    for(uint32_t i = 0; i < h; i++)
    {
        for(uint32_t j = 0; j < w; j++)
        {
            data1[i][j] = (*picture).data[i * w + j];
        }
    }

    for(uint32_t i = 0; i < h; i++)
    {
        for(uint32_t j = 0; j < w; j++)
        {
            data2[j][i] = data1[i][j];
        }
    }

    for(uint32_t j = 0; j < (w/2); j++)
    {
        for(uint32_t i = 0; i < h; i++)
        {
            struct pixel temp = data2[j][i];
            data2[j][i] = data2[w-j-1][i];
            data2[w-j-1][i] = temp;

        }
    }

    for(uint32_t i = 0; i < w; i++)
    {
        for(uint32_t j = 0; j < h; j++)
        {
            rotated_data[i * h + j] = data2[i][j];
        }
    }
    
    (*rotated_img).width = (*picture).heigth;
    (*rotated_img).heigth = (*picture).width;
    (*rotated_img).data = rotated_data;
    return rotated_img;
}

#endif