#ifndef _INPUT_FORMAT_H_
#define _INPUT_FORMAT_H_

#include "input_format.c"

struct bmp_file_header* normal_bmp_reader(char* );
struct image* extract_image_data(struct bmp_file_header* );

#endif