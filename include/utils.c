#ifndef _UTILS_C_
#define _UTILS_C_
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#define WRITE(ATTR, TYPE) fwrite(&(output_bmp.ATTR), sizeof(TYPE), 1, writestream)
#include "inside_format.h"
//#include <utils.c>

void save_as_bmp(struct image* out_img, char* filename)
{
    uint32_t w = (*out_img).width;
    uint32_t h = (*out_img).heigth;
    struct pixel* d = (*out_img).data;

    struct bmp_file_header output_bmp;
    uint8_t ppr = 4 - (((*out_img).width * 3) % 4);
    uint8_t padding = 0x00;
    uint32_t length = (*out_img).heigth * ((*out_img).width * 3 + ppr); 
    uint8_t out_data[length];
    uint32_t w_wp = w*3+ppr;
    uint32_t index = 0;
    for(uint32_t i = 0; i < length;)
    {
        for(uint32_t j = 0; j < w; j++)
        {
            out_data[i] = d[index].b;
            i++;
            out_data[i] = d[index].g;
            i++;
            out_data[i] = d[index].r;
            i++;
            index++;
        }
        for(uint32_t j = 0; j < ppr; j++)
        {
            out_data[i] = padding;
            i++;
        }
    }



    output_bmp.signature = 0x4d42;
    output_bmp.reserved1 = 0x0000;
    output_bmp.reserved2 = 0x0000;
    output_bmp.offset = 54;
    output_bmp.dib_size = 40;
    output_bmp.width = (*out_img).width;
    output_bmp.heigth = (*out_img).heigth;
    output_bmp.planes = 0x0001;
    output_bmp.bitcount = 24;
    output_bmp.compression = 0;
    output_bmp.image_size = length;
    output_bmp.XpelsPerMeter = 0;
    output_bmp.YpelsPerMeter = 0;
    output_bmp.colorsused = 0;
    output_bmp.colorsimportant = 0;
    output_bmp.ImageData = out_data;
    output_bmp.file_size = output_bmp.offset + output_bmp.image_size;

    FILE* writestream = fopen(filename,"wb");
    WRITE(signature, uint16_t);
    WRITE(file_size, uint32_t);
    WRITE(reserved1, uint16_t);
    WRITE(reserved2, uint16_t);
    WRITE(offset, uint32_t);
    WRITE(dib_size, uint32_t);
    WRITE(width, uint32_t);
    WRITE(heigth, uint32_t);
    WRITE(planes, uint16_t);
    WRITE(bitcount, uint16_t);
    WRITE(compression, uint32_t);
    WRITE(image_size, uint32_t);
    WRITE(XpelsPerMeter, uint32_t);
    WRITE(YpelsPerMeter, uint32_t);
    WRITE(colorsused, uint32_t);
    WRITE(colorsimportant, uint32_t);
    fseek(writestream, output_bmp.offset, SEEK_SET);
    for(uint32_t i = 0 ; i < length; i++)
    {
        fwrite(&(output_bmp.ImageData[i]), sizeof(uint8_t), 1, writestream);
    }
    fclose(writestream);
}

uint32_t le_to_normal_4B(uint32_t le)//convert little endian sequence to normal for human hex number (4bytes)
{
    return (((le & 0xFF) << 24) | ((le & (0xFF << 8)) << 8) | ((le & (0xFF << 16)) >> 8) | ((le & (0xFF << 24)) >> 24));
}

uint16_t le_to_normal_2B(uint16_t le)//convert little endian sequence to normal for human hex number (2bytes)
{
    return (((le & 0xFF) << 8) | ((le & (0xFF << 8)) >> 8));
}

void rewrite_new_bmp(char* filename_o, char* filename_r, struct image* rotated_image)
{
    uint8_t header[14]; 
    uint8_t padding = 0x00;
    uint32_t w = (*rotated_image).width;
    uint32_t h = (*rotated_image).heigth;
    struct pixel* data = (*rotated_image).data;
    FILE* readstream = fopen(filename_o,"rb");
    fread(&header, sizeof(uint8_t), 14, readstream);
    fclose(readstream);
    uint64_t file_offset = (header[13]<<(8*3))|(header[12]<<(8*2))|(header[11]<<8)|(header[10]);
    uint8_t fullheader[file_offset];
    readstream = fopen(filename_o,"rb");
    fread(&fullheader, sizeof(uint8_t), file_offset, readstream);
    fclose(readstream);

    for(uint8_t i = 0; i < 4; i++)
    {
        uint8_t t1 = fullheader[18+i];
        uint8_t t2 = fullheader[38+i];
        fullheader[18+i] = fullheader[22+i];
        fullheader[38+i] = fullheader[42+i];
        fullheader[22+i] = t1;
        fullheader[42+i] = t2;
    }

    uint32_t size = 54+(h*(w+(w%4)));
    fullheader[2] = size & (0xFF << 24);
    fullheader[3] = size & (0xFF << 16);
    fullheader[4] = size & (0xFF << 8);
    fullheader[5] = size & 0xFF;

    FILE* writestream = fopen(filename_r,"wb");
    fwrite(&fullheader, sizeof(uint8_t), file_offset, writestream);
    uint8_t cp = 0;
    if(((*rotated_image).width * 3) % 4 != 0)
    {
        cp += (((*rotated_image).width*3)%4);
    }
    for(uint64_t i = 0; i < (*rotated_image).heigth; i++)
    {
        for(uint64_t j = 0; j < (*rotated_image).width; j += 1)
        {
            fwrite(&((*rotated_image).data[i*(*rotated_image).heigth+j].b), sizeof(uint8_t), 1, writestream);
            fwrite(&((*rotated_image).data[i*(*rotated_image).heigth+j].g), sizeof(uint8_t), 1, writestream);
            fwrite(&((*rotated_image).data[i*(*rotated_image).heigth+j].r), sizeof(uint8_t), 1, writestream);
        }
        for(uint8_t j = 0; j < cp; j += 1)
        {
            fwrite(&padding, sizeof(uint8_t), 1, writestream);
        }
    }
    fclose(writestream);
}



int check(char* filename)
{
    FILE* file = fopen(filename,"rb+");
    if(!file)
    {
        //printf("file not exist: %d\n",file);
        fclose(file);
        return 1;
    }
    else
    {
        fclose(file);
        return 0;
    }
    //works perfect
}

void create_file(char* filename)
{
    FILE* new_file = fopen(filename,"ab+");
    if(!new_file)
    {
        printf("file not created\n");
    }
    fclose(new_file);
    //works perfect
}
#endif