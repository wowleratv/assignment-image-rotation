#ifndef _INPUT_FORMAT_C_
#define _INPUT_FORMAT_C_
#define BR_ printf("\tbr\n");
#define BR2_ printf("\t\tbr2\n");
#include "inside_format.h"
#include <stdio.h>
#include <stdint.h>
#include <malloc.h>
#include <stdlib.h>
#include "utils.h"

struct bmp_file_header* normal_bmp_reader(char* filename)
{
    struct bmp_file_header* read_image = (struct bmp_file_header*)malloc(sizeof(struct bmp_file_header));
    uint16_t buffer_2b;
    uint32_t buffer_4b;
    FILE* readstream;
    readstream = fopen(filename,"rb");
    if(!readstream)
    {
        printf("cant open file");
        exit(0);
    }
    //bmp-header
    fread(&buffer_2b, sizeof(uint16_t), 1, readstream);
    (*read_image).signature = buffer_2b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).file_size = buffer_4b;
    fread(&buffer_2b, sizeof(uint16_t), 1, readstream);
    (*read_image).reserved1 = buffer_2b;
    fread(&buffer_2b, sizeof(uint16_t), 1, readstream);
    (*read_image).reserved2 = buffer_2b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).offset = buffer_4b;
    //bmpinfo-header
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).dib_size = buffer_4b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).width = buffer_4b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).heigth = buffer_4b;
    fread(&buffer_2b, sizeof(uint16_t), 1, readstream);
    (*read_image).planes = buffer_2b;
    fread(&buffer_2b, sizeof(uint16_t), 1, readstream);
    (*read_image).bitcount = buffer_2b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).compression = buffer_4b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).image_size = buffer_4b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).XpelsPerMeter = buffer_4b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).YpelsPerMeter = buffer_4b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).colorsused = buffer_4b;
    fread(&buffer_4b, sizeof(uint32_t), 1, readstream);
    (*read_image).colorsimportant = buffer_4b;
    fseek(readstream, (*read_image).offset, SEEK_SET);
    (*read_image).ImageData = (uint8_t*)malloc((*read_image).image_size*sizeof(uint8_t));
    fread((*read_image).ImageData,sizeof(uint8_t),(*read_image).image_size, readstream);
    return read_image;
}

struct image* extract_image_data(struct bmp_file_header* picture)
{
    uint8_t ppr = ((*picture).image_size / (*picture).heigth) - ((*picture).width * 3);
    uint32_t width_wp = (*picture).width*3 + (uint32_t)ppr;
    
    struct pixel* image_data = (struct pixel*)malloc((*picture).width * (*picture).heigth * sizeof(struct pixel));

    uint32_t imd_index = 0;

    for(uint32_t i = 0; i < (*picture).heigth; i++)
    {
        for(uint32_t j = 0; j < (*picture).width*3; j+=3)
        {
            image_data[imd_index].b = (*picture).ImageData[i * width_wp + j + 0];
            image_data[imd_index].g = (*picture).ImageData[i * width_wp + j + 1];
            image_data[imd_index].r = (*picture).ImageData[i * width_wp + j + 2];
            imd_index++;
        }
    }

    struct image* extracted_image = (struct image*)malloc(1*sizeof(struct image));
    (*extracted_image).data = image_data;
    (*extracted_image).width = (*picture).width;
    (*extracted_image).heigth = (*picture).heigth;
    return extracted_image;
}

#endif