#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdint.h>
#include "include\include_all.h"

int main(int argc, char* argv[])
{
    char* input_filename;
    char* transformed_filename;

    if (argc < 3)
    {
        printf("Wrong count of arguments\n");
        printf("Syntax: main <input> <output>\n");
        return 1;        
    }
    else if(argc >= 3)
    {
        input_filename = argv[1];
        transformed_filename = argv[2];
    }
    
    printf("Name of file where I will save the transformed image: %s\n", transformed_filename);
    
    if (check(input_filename) == 1)
    {
        printf("Can`t found file such as %s\n", input_filename);
        return 1;
    }
    if (check(transformed_filename) == 1)
    {
        create_file(transformed_filename);
    }

    struct bmp_file_header* input_image = normal_bmp_reader(input_filename);
    //printf("%d\n", (*input_image).dib_size);
    struct image* ext_img = extract_image_data(input_image);
    struct image* rtd_img = image_rotation(ext_img);//поворот по часовой => один поворот против = 3 по часовой
    rtd_img = image_rotation(rtd_img);
    rtd_img = image_rotation(rtd_img);
    save_as_bmp(rtd_img, transformed_filename);
    free((*input_image).ImageData);
    free(input_image);
    free((*ext_img).data);
    free(ext_img);
    free((*rtd_img).data);
    free(rtd_img);
    return 0;
}